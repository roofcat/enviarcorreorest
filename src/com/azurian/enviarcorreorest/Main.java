package com.azurian.enviarcorreorest;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

public class Main {

	public static void main(String[] args) throws Exception {
		
		final String url = "http://azurian-rastreo.appspot.com/input";
		final String api_key = "AIzaSyBHKxr4aT1LZ-CygPMMR8c3NzGxb0teZz8";
		String user = "chilectra-azurian";
		String password = "estaEsLaClaveDeChilectra";
		String userPass = user + ":" + password;
		/*
		byte[] userBytes = Base64.encodeBase64(user.getBytes());
		byte[] passBytes = Base64.encodeBase64(password.getBytes());
		byte[] myHash = Base64.encodeBase64(userPass.getBytes());
		System.out.println( new String(userBytes) );
		System.out.println( new String(passBytes) );
		System.out.println( new String(myHash) );
		*/
		for (int i = 7669; i < 10000; i++) {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(objectToJson(Integer.toString(i)));
			//httpPost.setHeader("Authorization", "Basic " + api_key);
			
			HttpResponse res = client.execute(httpPost);
			System.out.println(res.getStatusLine().getStatusCode());
			//System.out.println(res.getEntity());
			System.out.println(i);
		}
	}
	
	public static HttpEntity objectToJson(String id)
	{
		// Archivos para adjuntar
		File miXml = new File("C:\\Users\\crojas\\workspace\\EnviarCorreoREST\\src\\fact.xml");
		File miPdf = new File("C:\\Users\\crojas\\workspace\\EnviarCorreoREST\\src\\basico.pdf");
		
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addTextBody("enterprise", "azurian");
		builder.addTextBody("campaign_id", id);
		builder.addTextBody("email", "admin@gapps.azurian.com");
		builder.addTextBody("recipient_type", "cliente");
		builder.addTextBody("full_name", "christian diego");
		builder.addTextBody("subject", "Estado de cuenta: ");
		builder.addTextBody("htmlBody", "<div align='center'> Hola este es un correo de prueba </div>");
		builder.addBinaryBody("file1", miXml);
		builder.addBinaryBody("file2", miPdf);
		
		return builder.build();
	}
}
